var login = require('./LoginPage');

describe('login page', function() {
  it('should show a success message when user successfully logged', function() {
    login.visit();
    login.enterUserName();
    login.enterPassWord();
    login.submit();
    expect(login.getMessage()).toContain("You logged into a secure area!");
  });

  it('should show an invalid user name message when user enters wrong user name', function(){
    login.visit();
    login.enterUserName("Wrong");
    login.enterPassWord();
    login.submit();
    expect(login.getMessage()).toContain("Your username is invalid!");
  });

  it('should show an invalid password message when user enters wrong password', function(){
    login.visit();
    login.enterUserName();
    login.enterPassWord("Wrong");
    login.submit();
    expect(login.getMessage()).toContain("Your password is invalid!");
  });

});