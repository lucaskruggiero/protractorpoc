var login = function() {
  browser.ignoreSynchronization = true;
  var url = "http://the-internet.herokuapp.com/login";
  var userNameField = element(by.id("username"));
  var passWordField = element(by.id("password"));
  var form = element(by.id("login"));
  var message = element(by.css(".flash"));
  var userName = "tomsmith";
  var passWord = "SuperSecretPassword!";


  this.visit = function() {
    browser.get(url);
  };

  this.enterUserName = function(name = userName) {
    userNameField.sendKeys(name);
  };

  this.enterPassWord = function(password = passWord) {
    passWordField.sendKeys(password);
  };

  this.submit = function() {
    form.submit();
  };

  this.getMessage = function(){
    return message.getText();
  }
};
module.exports = new login();
